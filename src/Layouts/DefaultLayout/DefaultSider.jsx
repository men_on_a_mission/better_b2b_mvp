import React, { Component } from "react";
import { Layout, Menu, Icon } from "antd";
import logo from "../../assets/brand/logo.png";
import { Link } from "react-router-dom";
import "./styles.scss";
import { connect } from "react-redux";
import { t } from 'ttag';

import _colors from '../../custom_theme/_colors'

const { Sider } = Layout;
const { Item } = Menu;

class DefaultSider extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    console.log(this.props.collapsed)
    return (
      <Sider
        trigger={null}
        collapsible
        collapsed={this.props.collapsed}
        theme='light'
        className="sideMenu"
      >
        <Link to="/home">
          <div className="logoContainer">
            <img
              className="logo"
              alt="logo"
              src={logo}
              style={{
                width: 'auto',
                height: this.props.collapsed ? '20px' : '34px'
              }}
            />
            {/* {!this.props.collapsed ? this.props.salon.name : null} */}
          </div>
        </Link>
        <Menu
          mode="inline"
          defaultSelectedKeys={["1"]}
        >
          {/* Section */}
          {
            !this.props.collapsed
              ? <Item disabled> <h3>{t`Operations`}</h3> </Item>
              : null
          }

          <Item key="schedule">
            <Link to="/home/schedule">
              <Icon type="calendar" style={{ color: _colors.GREEN, fontSize: '17px' }} />
              <span>{t`My schedule`}</span>
            </Link>
          </Item>

          <Item key="inventroy">
            <Link to="/home/inventory">
              <Icon type="barcode" style={{ color: _colors.YELLOW, fontSize: '17px' }} />
              <span>{t`Inventory`}</span>
            </Link>
          </Item>

          <Item key="staffMembers">
            <Link to="/home/staffMembers">
              <Icon type="team" style={{ color: _colors.BLUE, fontSize: '17px' }} />
              <span>{t`Team Members`}</span>
            </Link>
          </Item>

          <Item key="visitors">
            <Link to="/home/visitors">
              <Icon type="smile" style={{ color: _colors.PINK, fontSize: '17px' }} />
              <span>{t`Visitors`}</span>
            </Link>
          </Item>

          <Item key="services">
            <Link to="/home/services">
              <Icon type="tags" style={{ color: _colors.ORANGE, fontSize: '17px' }} />
              <span>{t`Services`}</span>
            </Link>
          </Item>

          {/* Section */}

          {
            !this.props.collapsed
              ? <Item disabled> <h3>{t`Salon`}</h3> </Item>
              : null
          }

          <Item key="dashboard">
            <Link to="/home/dashboard">
              <Icon type="pie-chart" style={{ color: _colors.GREEN, fontSize: '17px' }} />
              <span>{t`Dashboard`}</span>
            </Link>
          </Item>

          <Item key="dataHub">
            <Link to="/home/dataHub">
              <Icon type="bar-chart" style={{ color: _colors.YELLOW, fontSize: '17px' }} />
              <span>{t`Data Hub`}</span>
            </Link>
          </Item>

          <Item key="accounting">
            <Link to="/home/accounting">
              <Icon type="account-book" style={{ color: _colors.BLUE, fontSize: '17px' }} />
              <span>{t`Accounting`}</span>
            </Link>
          </Item>

          <Item key="preferences">
            <Link to="/home/preferences">
              <Icon type="setting" style={{ color: _colors.PINK, fontSize: '17px' }} />
              <span>{t`Preferences`}</span>
            </Link>
          </Item>
        </Menu>
      </Sider>
    );
  }
}

export default connect(
  state => state.common,
  null
)(DefaultSider);

import React, { Component } from "react";
import {
  Layout,
  Icon,
  Drawer,
  Avatar,
  Divider,
  Button,
  Row,
  Col,
  Dropdown,
  Menu
} from "antd";
import { withRouter, Link } from "react-router-dom";
import "./styles.scss";
import { connect } from "react-redux";
import { userActionCreators } from "../../store/user-store";
import { authService } from "../../services/auth-service";
import { saveLocale } from '../../i18n/i18nInit';
import { t } from 'ttag'

const { Header } = Layout;

const setLocale = (locale) => {
  saveLocale(locale);
  window.location.reload();
}

class DefaultHeader extends Component {
  state = {
    drawerVisible: false
  };

  showDrawer = () => {
    this.setState({ drawerVisible: true });
  };

  hideDrawer = () => {
    this.setState({ drawerVisible: false });
  };

  logOutUser() {
    authService.logout();
    this.props.setUser({}, false);
  }

  render() {
    let user = this.props.user.user || {};
    let country = user["https://better.com/country"];
    const menu = (
      <Menu>
        <Menu.Item key="0">
          <Link to="/home/myProfile">
            <Icon type="user" style={{ marginRight: "10px" }} />
            {t`My Profile`}
          </Link>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="2" onClick={this.logOutUser}>
          <Icon type="logout" />
          {t`Sign Out`}
        </Menu.Item>
      </Menu>
    );
    return (
      <Header className="layoutHeader">
        <div className='headerActions'>
          <div className='headerLeft'>
            <Icon
              type={this.props.siderCollapsed ? "menu-unfold" : "menu-fold"}
              onClick={this.props.toggleSider}
              style={{ fontSize: "18px" }}
            />
          </div>

          <div className='headerRight'>
            <Dropdown overlay={menu} trigger={["hover"]}>
              <div className="drawerToggler">
                <Avatar
                  shape="circle"
                  icon="user"
                  src={user.picture}
                  style={{ marginRight: "8px" }}
                />
                {user.nickname}
                <Icon type="down" style={{ marginLeft: "4px" }} />
              </div>
            </Dropdown>

            <Dropdown overlay={langMenu}>
              <Button type='link'>
                <i className="fas fa-globe"></i>
              </Button>
            </Dropdown>
          </div>
        </div>

        <Drawer
          placement="right"
          closable={true}
          onClose={this.hideDrawer}
          visible={this.state.drawerVisible}
          width={400}
        >
          <Row gutter={32}>
            <Col span={8} style={{ maxHeight: "100px" }}>
              <img src={user.picture} alt="UserPhoto" />
            </Col>
            <Col span={16}>
              <h3 className="drawerTitle">{user.name}</h3>
              <p className="drawerSubtitle">Country: {country}</p>
              <p className="drawerSubtitle">
                Salon: {this.props.common.salon.name}
              </p>
              <Button type="primary" onClick={this.logOutUser}>
                Logout
              </Button>
            </Col>
          </Row>
          <Divider />
        </Drawer>
      </Header>
    );
  }
}

const langMenu = (
  <Menu>
    <Menu.Item key="0" onClick={() => setLocale('bg')}>
      {t`Bulgarian`}
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item key="1"onClick={() => setLocale('en')}>
      {t`English`}
    </Menu.Item>
  </Menu>
)

export default connect(
  state => state,
  userActionCreators
)(withRouter(DefaultHeader));

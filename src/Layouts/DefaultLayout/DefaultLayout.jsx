import React, { Component } from "react";
import { Layout } from "antd";
import { withRouter } from "react-router-dom";
import "./styles.scss";
import { connect } from "react-redux";
import { commonActionCreators } from "../../store/common-store";
import { getSalon } from "../../services/better-api-service";
import { salonId } from "../../models/user-claims";

// Layouts
import DefaultHeader from "./DefaultHeader";
import DefaultSider from "./DefaultSider";

const { Content } = Layout;

class DefaultLayout extends Component {
  constructor(props) {
    super(props);

    this.state = {
      siderCollapsed: false,
      drawerVisible: false
    };
  }

  async componentDidMount() {
    if (!this.props.common.salon) {
      this.props.setLoading(true);
      const salon = await getSalon(this.props.user.user[salonId]);
      if (!salon) {
        this.props.history.push("/salonSetup");
      } else {
        this.props.setSalon(salon);
      }
      this.props.setLoading(false);
    }
  }

  toggleSider = () => {
    this.setState({ siderCollapsed: !this.state.siderCollapsed });
  };

  render() {
    const { salon } = this.props.common;
    return (
      <div>
        {salon && (
          <Layout style={{ height: "100vh" }}>
            <DefaultSider collapsed={this.state.siderCollapsed} />
            <Layout>
              <DefaultHeader
                toggleSider={() => this.toggleSider()}
                siderCollapsed={this.state.siderCollapsed}
              />
              <Content >{this.props.children}</Content>
            </Layout>
          </Layout>
        )}
      </div>
    );
  }
}

export default connect(
  state => state,
  commonActionCreators
)(withRouter(DefaultLayout));

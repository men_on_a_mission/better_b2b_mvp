import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";

export default class ProtectedRoute extends Component {
  render() {
    const { isLoggedIn } = this.props;
    const hasSession = localStorage.getItem('isLoggedIn');
    return (
      <div>
        {isLoggedIn ? (
          <Route {...this.props} />
        ) : (
          !hasSession &&
          <Redirect
            to={{
              pathname: "/authentication",
              state: { referrer: this.props.location.pathname }
            }}
          />
        )}
      </div>
    );
  }
}

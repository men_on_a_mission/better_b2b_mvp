import React, { Component } from "react";
import { Icon, Input, } from "antd";

import './styles.scss'
import _colors from '../../custom_theme/_colors.js'
import propTypes from 'prop-types'

class SearchBar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            style: {
                width: this.props.width ? this.props.width : null,
                backgroundColor: this.props.bgColor ? this.props.bgColor : 'transparent'
            },
            iconSize: this.props.iconSize ? this.props.iconSize : '14px',
            iconColor: this.props.iconColor ? this.props.iconColor : _colors.ORANGE,
            bgColor: this.props.bgColor ? this.props.bgColor : 'transparent',
            textSize: this.props.textSize ? this.props.textSize : null,

        }
    }

    render() {
        return (
            <div className='appSearchBar' style={{ ...this.state.style, ...this.props.style }}>
                <Icon style={{ color: this.state.iconColor, fontSize: this.state.iconSize }} type='search' />
                <Input
                    className='appSearchBarInput'
                    placeholder={this.props.placeholder}
                    style={{ fontSize: this.state.textSize }}
                    onChange={(e) => this.props.onChange(e)}
                />
            </div>
        )
    }
}


SearchBar.propTypes = {
    placeholder: propTypes.string,
    width: propTypes.string,
    iconSize: propTypes.string,
    iconColor: propTypes.string,
    bgColor: propTypes.string,
    textSize: propTypes.string,
    onChange: propTypes.func

}


export default SearchBar;

import React from "react";
import { Icon, Tooltip, Badge, Card, Typography } from "antd";
import { t } from 'ttag'
import './styles.scss'
import _colors from '../../custom_theme/_colors.js'

// Auto generates avatar
import Avatar from 'react-avatar'

const { Title } = Typography;
const { Meta } = Card

// Gets the badge style based on a nubmer filter
const getBadge = count => {
    const w = {
        backgroundColor: '#fff',
        color: '#999',
        boxShadow: '0 0 0 1px #d9d9d9 inset'
    }
    return count < 10 ? w
        : count < 50 ? { backgroundColor: _colors.YELLOW }
            : count < 100 ? { backgroundColor: _colors.BLUE }
                : { backgroundColor: _colors.GREEN }
}

const VisitorCard = (props) => {
    let { visitor, edit, createBooking } = props
    return (
        <Card
            className="visitorCardWrapper"
            actions={[
                <Tooltip title={t`Quick edit`}>
                    <Icon type="edit" key="edit" onClick={() => edit('edit', visitor)} />
                </Tooltip>,
                <Tooltip title={t`Create a booking`}>
                    <Icon type="calendar" key="bookIn" onClick={() => createBooking(visitor)} />
                </Tooltip>,
                // <Tooltip title={t`Go to profile`}><Icon type="user" key="userProfile" /></Tooltip>,
            ]}>
            <div className='customBadge'><div>{visitor.visits}</div></div>
            <Badge overflowCount={1000} count={visitor.visits} style={getBadge(visitor.visits)}>
                <Avatar name={visitor.name} size={128} />
            </Badge>
            <Title level={4} style={{ marginTop: "10px" }}>{visitor.name}</Title>
            <p className='noMargin'><i className="fas fa-phone" />&nbsp;&nbsp;{visitor.phoneNumber}</p>
            {
                visitor.email
                    ? <p className='noMargin'><i className="fas fa-envelope" />&nbsp;&nbsp;{visitor.email}</p>
                    : <p className='noMargin'><i className="fas fa-envelope" />&nbsp;&nbsp; ...</p>
            }
        </Card>
    )
}


const VisitorCardCompact = (props) => {
    let { visitor, edit, createBooking } = props
    return (
        <Card
            className="visitorCardWrapperCompact"
            actions={[
                <Tooltip title={t`Quick edit`}>
                    <Icon type="edit" key="edit" onClick={() => edit('edit', visitor)} />
                </Tooltip>,
                <Tooltip title={t`Create a booking`}>
                    <Icon type="calendar" key="bookIn" onClick={() => createBooking(visitor)} />
                </Tooltip>,
                // <Tooltip title={t`Go to profile`}><Icon type="user" key="userProfile" /></Tooltip>,
            ]}>

            <Meta
                avatar={
                    <Badge overflowCount={1000} count={visitor.visits} style={getBadge(visitor.visits)}>
                        <Avatar round name={visitor.name} size={40} />
                    </Badge>
                }
                title={visitor.name}
                description={
                    <div>
                        <p className='cardText'><i className="fas fa-phone" />&nbsp;&nbsp;{visitor.phoneNumber}</p>
                        {
                            visitor.email
                                ? <p className='cardText'><i className="fas fa-envelope" />&nbsp;&nbsp;{visitor.email}</p>
                                : <p className='cardText'><i className="fas fa-envelope" />&nbsp;&nbsp; ...</p>
                        }
                    </div>
                }>
            </Meta>
        </Card>
    )
}


export { VisitorCard, VisitorCardCompact }

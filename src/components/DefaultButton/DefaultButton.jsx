import React, { Component } from "react";
import { Icon, Typography } from "antd";

import './styles.scss'
import _colors from '../../custom_theme/_colors.js'
import propTypes from 'prop-types'

const { Text } = Typography;

class DefaultButton extends Component {
    constructor(props) {
        super(props)
        this.state = {
            style: {
                width: this.props.width ? this.props.width : null,
                height: this.props.height ? this.props.height : '30px',
                backgroundColor: this.props.color ? this.props.color : _colors.ORANGE,
            },
            iconColor: this.props.iconColor ? this.props.iconColor : _colors.WHITE,
            fontColor: this.props.fontColor ? this.props.fontColor : _colors.WHITE,

        }
    }

    render() {
        return (
            <div 
                className='flexCenter appDefaultButton' 
                style={{ ...this.state.style, ...this.props.style }} 
                onClick={this.props.onClick}
            >
                <div className='flexCenter appDefaultButtonInputWrapper'>
                    <Icon style={{color: this.state.iconColor, fontSize: '15px' }} type='arrow-right' />
                </div>
                <div className='flexCenter appDefaultButtonTextWrapper'>
                    <Text className='appDefaultButtonText' style={{ color: this.state.fontColor }} strong>
                        {this.props.text.toUpperCase()}
                    </Text>
                </div>
            </div>
        )
    }
}


DefaultButton.propTypes = {
    className: propTypes.string,
    text: propTypes.string,
    width: propTypes.string,
    height: propTypes.string,
    iconColor: propTypes.string,
    background: propTypes.string,
    color: propTypes.string,
    fontColor: propTypes.string,
    onClick: propTypes.func

}


export default DefaultButton;

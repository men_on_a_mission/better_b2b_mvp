import React, { Component } from "react";
import { Icon, Typography, Tooltip } from "antd";

import './styles.scss'
import _colors from '../../custom_theme/_colors.js'
import propTypes from 'prop-types'

const { Text } = Typography;

class DefaultCard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            style: {
                width: this.props.width ? this.props.width : null,
                height: this.props.height ? this.props.height : null,
                backgroundColor: this.props.bg ? this.props.bg : _colors.WHITE,
            },
            themeColor: this.props.color ? this.props.color : _colors.ORANGE,
            headerHeight: this.props.headerHeight ? this.props.headerHeight : '32px',
            fontSize: this.props.fontSize ? this.props.fontSize : '12px'

        }
    }

    render() {
        return (
            <div className='appDefaultCard shadowLight' style={{ ...this.state.style, ...this.props.style }}>
                <div
                    className='appDefaultCardHeader'
                    style={{
                        height: this.state.headerHeight,
                        padding: '0 16px',
                        borderBottom: `2px solid ${this.state.themeColor}`,
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between'
                    }}
                >
                    <Text strong style={{ color: _colors.TITLE, fontSize: this.state.fontSize }}>{this.props.title}</Text>

                    <Tooltip placement="top" title={this.props.info}>
                        <Icon 
                            className='appDefaultCardInfoIcon' 
                            type='info-circle' 
                            style={{ color: this.state.themeColor, fontSize: '12px' }} 
                        />
                    </Tooltip>
                </div>

                <div className='appDefaultCardBody' style={{flexGrow: '1',padding: '16px'}}>
                    {this.props.children}
                </div>
            </div>
        )
    }
}


DefaultCard.propTypes = {
    className: propTypes.string,
    width: propTypes.string,
    title: propTypes.string,
    info: propTypes.string,
    height: propTypes.string,
    color: propTypes.string,
    bg: propTypes.string,
    fontSize: propTypes.string,
    children: propTypes.node

}


export default DefaultCard;

import React from "react";
import { Icon, Spin } from 'antd'
import { t } from 'ttag'
import './styles.scss'

const LoadingOverlay = () => (
    <div className='loadingOverlay'>
        <Spin
            spinning
            tip={t`Loading...`}
            size="large"
            indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
            className="loadingOverlay"
        />

    </div>
)

export default LoadingOverlay;

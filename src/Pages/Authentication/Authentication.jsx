import React, { Component } from "react";
import { connect } from "react-redux";
import { commonActionCreators } from "../../store/common-store";
import { userActionCreators } from "../../store/user-store";
import { authService } from "../../services/auth-service";

class Authentication extends Component {
  async componentDidMount() {
    if (localStorage.getItem("isLoggedIn") !== "true") {
      await this.authenticateUser();
    }
  }

  async authenticateUser() {
    this.props.setLoading(true);

    const isLoggedIn = await authService.authenticateUser();
    let user;
    if (isLoggedIn) {
      user = await authService.getUserProfile();
      this.props.setUser(user, isLoggedIn);

      const redirectUrl = localStorage.getItem("redirectUrl");
      if (redirectUrl){
        localStorage.removeItem('redirectUrl');
        this.props.history.push(redirectUrl);
      }

      this.props.setLoading(false);
    } else {
      let { state } = this.props.location;
      if (state && state.referrer === "/signup") {
        authService.signup();
      } else {
        state && localStorage.setItem("redirectUrl", state.referrer);
        authService.login();
      }
    }
  }

  render() {
    return <div />;
  }
}

export default connect(
  null,
  { ...commonActionCreators, ...userActionCreators }
)(Authentication);

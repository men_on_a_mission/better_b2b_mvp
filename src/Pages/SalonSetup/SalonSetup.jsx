import React, { Component } from "react";
import { Form, Input, Button } from "antd";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { commonActionCreators } from "../../store/common-store";
import Animate from "rc-animate";
import "./styles.scss";
import { createSalon } from "../../services/better-api-service";
import { t } from 'ttag'

const { Item } = Form;

class SalonSetup extends Component {
  state = {
    loading: false
  };

  createSalon = (e) => {
    e.preventDefault();
    this.setState({ loading: true });

    const { user } = this.props;
    this.props.form.validateFields(async (err, salon) => {
      if (!err) {
        const salonId = user['https://better.com/salon_id'];
        const newSalon = await createSalon({id: salonId, name: salon.salonName})
        if (newSalon){
          this.props.setSalon(newSalon);
          this.props.history.push('/home');
        }
      }
    });
    this.setState({ loading: false });
  }

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 10 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 10 },
        sm: { span: 5 }
      }
    };
    const { getFieldDecorator } = this.props.form;
    return (
      <Animate transitionAppear transitionName="fade">
        <div className="center-content salon-setup">
          <Form {...formItemLayout} className="salon-setup-form">
            <Item label={t`Salon Name`}>
              {getFieldDecorator("salonName", {
                rules: [
                  {
                    required: true,
                    message: t`Please input your salon name!`
                  }
                ]
              })(<Input />)}
            </Item>
            <Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                style={{ width: "100" }}
                onClick={this.createSalon}
                loading={this.state.loading}
              >
                Create
              </Button>
            </Item>
          </Form>
        </div>
      </Animate>
    );
  }
}

const WrappedSalonSetupForm = Form.create({ name: "salonRegister" })(
  SalonSetup
);

export default connect(
  state => state.user,
  commonActionCreators
)(withRouter(WrappedSalonSetupForm));

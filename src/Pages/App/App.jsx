import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Spin, Icon } from "antd";

import "./styles.scss";
import { appRoutes } from "../../routes";
import { commonActionCreators } from "../../store/common-store";
import { userActionCreators } from "../../store/user-store";
import { authService } from "../../services/auth-service";
import { t } from 'ttag';

class App extends Component {
  async componentDidMount() {
    if (this.props.user.isLoggedIn || localStorage.getItem("isLoggedIn") !== "true") return;

    await this.renewUserSession();
  }

  async renewUserSession() {
    this.props.setLoading(true);

    const isLoggedIn = await authService.renewSession();
    let user;
    if (isLoggedIn) {
      user = await authService.getUserProfile();
    }

    this.props.setUser(user, isLoggedIn);

    this.props.setLoading(false);
  }

  render() {
    let { isLoading } = this.props.common;
    const icon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
    return (
      <div>
        {isLoading && (
          <Spin
            spinning={isLoading}
            tip={t`Loading...`}
            size="large"
            indicator={icon}
            className="center-content loader"
          />
        )}
        <div>{appRoutes(this.props.user.isLoggedIn)}</div>
      </div>
    );
  }
}

export default connect(
  state => state,
  { ...commonActionCreators, ...userActionCreators }
)(withRouter(App));

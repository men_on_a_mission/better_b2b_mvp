import React, { Fragment } from 'react';
import { Row, Col, Card } from 'antd';
import './styles.scss'
import Register from '../../Views/Register'

import logo from '../../assets/brand/logo.png'

const CardHeader = () => (
   <div className='card-header'>
      <img className='card-header-logo' alt="Better logo" src={logo} />
   </div>
)

class NewUserOnboarding extends React.Component {
   constructor(props) {
      super(props)

      this.state = {}
   }

   render() {
      return (
         <Fragment>
            <Row type="flex" justify="center" style={{ padding: 64 }}>
               <Col span={12}>
                  <Card 
                     hoverable={true} 
                     title='Регистриране на нов потребител' 
                     cover={<CardHeader />}
                     headStyle={{borderBottom: 'none'}}
                  >
                     <Register />
                  </Card>
               </Col>
            </Row>
         </Fragment>
      );
   }
}

export default NewUserOnboarding

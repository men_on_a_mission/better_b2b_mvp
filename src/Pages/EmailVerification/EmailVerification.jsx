import React, { Component } from "react";
import { Card } from "antd";
import logo from "../../assets/brand/logo.png";
import Animate from "rc-animate";

import "./styles.scss";

export default class EmailVerification extends Component {
  render() {
    return (
      <Animate transitionAppear transitionName="fade">
        <div className="center-content email-verification">
          <Card title="Signup successful!" className="email-verification-card">
            Congradulations! You have successfully signed up for <b>Better</b>.
            Please verify your email address to continue using our application
            and start creating your salon.
            <div className="logo-container">
              <img alt="logo" src={logo} className="signet" />
            </div>
          </Card>
        </div>
      </Animate>
    );
  }
}

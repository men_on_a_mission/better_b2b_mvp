import { Reducer, Action, Dispatch } from "redux";
import { Salon } from "../models/salon";

export interface CommonState {
    isLoading: boolean;
    salon: Salon;
}

const setLoading = (isLoading: boolean) => {
    return (dispatch: Dispatch<SET_LOADING>): Action => {
        return dispatch({
            type: "SET_LOADING",
            payload: isLoading
        });
    };
};

const setSalon = (salon: Salon) => {
  return (dispatch: Dispatch<SET_SALON>): Action => {
      return dispatch({
          type: "SET_SALON",
          payload: salon
      });
  };
};

export const commonActionCreators = {
    setLoading: setLoading,
    setSalon: setSalon
};


interface SET_LOADING {
    type: "SET_LOADING";
    payload: boolean;
}

interface SET_SALON {
  type: "SET_SALON";
  payload: Salon;
}

type CommonAction = SET_LOADING | SET_SALON;

export const commonReducer: Reducer<CommonState, CommonAction> = (
    state: CommonState | undefined,
    action: CommonAction
) => {
    switch (action.type) {
        case "SET_LOADING":
          return Object.assign({}, state, {
            isLoading: action.payload
          });
        case "SET_SALON":
          return Object.assign({}, state, {
            salon: action.payload
          });
        default:
            return state || {} as CommonState;
    }
};

import { UserState, userReducer } from './user-store';
import { CommonState, commonReducer } from './common-store';

export interface State {
  user: UserState,
  common: CommonState
}

export const reducers: any = {
  user: userReducer,
  common: commonReducer
};

export interface AppThunkAction<TAction> {
  (dispatch: (action: TAction) => void, getState: () => State): void;
}
import { Reducer } from "redux";
import { Auth0UserProfile } from "auth0-js";

export interface UserState {
  user: Auth0UserProfile;
  isLoggedIn: boolean;
}

export const userActionCreators = {
  setUser: (user: Auth0UserProfile, isLoggedIn: boolean = false)  =>
    {
      return {
        type: "CHANGED_USER",
        payload: { user, isLoggedIn: isLoggedIn }
      } as CHANGED_USER
    }
};

interface CHANGED_USER {
  type: "CHANGED_USER";
  payload: UserState;
}

type UserAction = CHANGED_USER;

export const userReducer: Reducer<UserState, UserAction> = (
  state: UserState | undefined,
  action: UserAction
) => {
  switch (action.type) {
    case "CHANGED_USER":
      return {
        user: action.payload.user,
        isLoggedIn: action.payload.isLoggedIn
      };
    default:
      return state || {} as UserState;
  }
};

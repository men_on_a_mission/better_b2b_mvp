import { apiService } from "./api-service";
import { Salon } from "../models/salon";
import { notification } from "antd";
import { AxiosResponse } from "axios";
import { Error } from "../models/error";
import { StaffMember } from "../models/staff-member";
import { Service } from "../models/service";
import { Visitor } from "../models/visitor";

notification.config({
  placement: 'bottomRight'
});

export const getSalon = async (salonId: string): Promise<Salon> => {
  const response = await apiService.get<Salon>(`salon/${salonId}`);
  if (!response){
    return null;
  } else if (isSuccessful(response)){
    return response.data;
  } else {
    if (response.status !== 404)
      showErrors(response);

    return null;
  }
};

export const createSalon = async (salon: Salon): Promise<Salon> => {
  return await executeRequest(apiService.post("/salon", {
    id: salon.id,
    name: salon.name
  }));
}

// Services
export const getServices = async (salonId: string): Promise<any[]> => {
  let services = await executeRequest(apiService.get(`/salon/${salonId}/services`));
  return services
}

export const createService = async (salonId: string, service: Service): Promise<Service> => {
  return await executeRequest(apiService.post(`/salon/${salonId}/services`, service));
}

export const editService = async (salonId: string, serviceId: string, service: Service): Promise<Service> => {
  return await executeRequest(apiService.put(`/salon/${salonId}/services/${serviceId}`, service));
}

export const deleteService = async (salonId: string, serviceId: string): Promise<any> => {
  return await executeRequest(apiService.delete(`/salon/${salonId}/services/${serviceId}`));
}

// Visitors
export const getVisitors = async (salonId: string): Promise<any[]> => {
  let visitors = await executeRequest(apiService.get(`/salon/${salonId}/clients`));
  return visitors
}

export const createVisitor = async (salonId: string, visitor: Visitor): Promise<Service> => {
  return await executeRequest(apiService.post(`/salon/${salonId}/clients`, visitor));
}

export const editVisitor = async (salonId: string, visitorId: string, visitor: Visitor): Promise<Service> => {
  return await executeRequest(apiService.put(`/salon/${salonId}/clients/${visitorId}`, visitor));
}

export const deleteVisitor = async (salonId: string, visitorId: string): Promise<any> => {
  return await executeRequest(apiService.delete(`/salon/${salonId}/clients/${visitorId}`));
}

// Staff Members
export const getStaffMembers = async (salonId: string): Promise<StaffMember[]> => {
  return await executeRequest(apiService.get(`/salon/${salonId}/staffMembers`));
}


export const getStaffMember = async (salonId: string, sfId: string): Promise<StaffMember[]> => {
  return await executeRequest(apiService.get(`/salon/${salonId}/staffMembers/${sfId}`));
}


export const editStaffMember = async (salonId: string, sfId: string, sm: StaffMember): Promise<StaffMember[]> => {
  return await executeRequest(apiService.put(`/salon/${salonId}/staffMembers/${sfId}`, sm));
}

export const createStaffMember = async (salonId: string, staffMember: StaffMember): Promise<StaffMember> => {
  return await executeRequest(apiService.post(`/salon/${salonId}/staffMembers`, staffMember));
}

export const inviteStaffMember = async (salonId: string, staffMember: StaffMember): Promise<StaffMember> => {
  return await executeRequest(apiService.post(`/salon/${salonId}/staffMembers/invite/${staffMember.id}`, {
    email: staffMember.email,
    firstName: staffMember.firstName,
    lastName: staffMember.lastName
  }));
}

export const deleteStaffMember = async (salonId: string, staffMemberId: string): Promise<any> => {
  return await executeRequest(apiService.delete(`/salon/${salonId}/staffMembers/${staffMemberId}`));
}

const executeRequest = async (request: Promise<AxiosResponse>) => {
  const response = await request;
  if (!response)
    return null;

  if (isSuccessful(response)) {
    return response.data;
  } else if (isError(response)) {
    showErrors(response);
    return null;
  } else{
    return null;
  }
};


// Error Handling
const isSuccessful = (response: AxiosResponse): boolean => {
  return response.status >= 200 && response.status <= 299;
};

const isError = (response: AxiosResponse): boolean => {
  return response.status >= 400 && response.status <= 599;
};

const showErrors = (response: AxiosResponse) => {
  const errorMessage = response.statusText;
  const errors = response.data.errors;
  if (errors && errors.length > 0) {
    errors.forEach((error: Error) => {
      const args = {
        message: errorMessage,
        description: `${error && error.detail}`,
        duration: 10
      };

      if (error.code >= 400 && error.code <= 499)
        notification.warning(args);
      else
        notification.error(args);
    });
  } else {
    notification.error({
      message: errorMessage,
      description: response.data ? response.data : `Error at URL: ${response.config.url}`,
      duration: 3
    });
  }
}

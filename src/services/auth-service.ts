import auth0, {
  Auth0DecodedHash,
  Auth0UserProfile,
  Auth0Error
} from "auth0-js";

class AuthService {
  private _accessTokenKey = "access_token";
  private _idTokenKey = "id_token";
  private _accessToken?: string;
  private _idToken?: string;
  private _expiresAt = 0;
  private _userProfile?: Auth0UserProfile;

  auth0 = new auth0.WebAuth({
    domain: "better-social.eu.auth0.com",
    clientID: "CrNDAU6YB6nkN0pA2O41c55PXLwq9KZO",
    redirectUri: "http://localhost:3000/authentication",
    audience: "http://localhost:5000",
    responseType: "token id_token",
    scope: "openid profile"
  });

  login = () => {
    this.auth0.authorize();
  };

  signup = () => {
    this.auth0.authorize({
      mode: "signUp"
    });
  };

  authenticateUser = (): Promise<boolean> => {
    return new Promise(resolve => {
      this.auth0.parseHash((err, authResult) => {
        if (authResult && authResult.accessToken && authResult.idToken) {
          this.setSession(authResult);
          return resolve(true);
        } else if (err) {
          console.log(err);
          return resolve(false);
        } else {
          return resolve(false);
        }
      });
    });
  };

  getAccessToken = () => {
    return this._accessToken;
  };

  getIdToken() {
    return this._idToken;
  }

  getUserProfile = (): Promise<Auth0UserProfile> => {
    return new Promise(resolve => {
      if (!this._userProfile) {
        this.auth0.client.userInfo(
          this._accessToken || "",
          (error: Auth0Error | null, profile: Auth0UserProfile) => {
            if (profile) {
              this._userProfile = profile;
              resolve(profile);
            } else if (error) {
              console.log(error);
              resolve({} as Auth0UserProfile);
            } else {
              resolve({} as Auth0UserProfile);
            }
          }
        );
      } else {
        resolve(this._userProfile);
      }
    });
  };

  setSession = (authResult: Auth0DecodedHash | null) => {
    if (authResult) {
      localStorage.setItem(this._accessTokenKey, authResult.accessToken || "");
      localStorage.setItem(this._idTokenKey, authResult.idToken || "");
      let expiresAt =
        authResult.expiresIn &&
        authResult.expiresIn * 1000 + new Date().getTime();
      this._accessToken = authResult.accessToken;
      this._idToken = authResult.idToken;
      this._expiresAt = expiresAt ? expiresAt : 0;

      localStorage.setItem("isLoggedIn", "true");
    }
  };

  renewSession = (): Promise<boolean> => {
    return new Promise(resolve => {
      this.auth0.checkSession({}, async (err, authResult) => {
        if (authResult && authResult.accessToken && authResult.idToken) {
          this.setSession(authResult);
          resolve(true);
        } else if (err) {
          console.log(err);
          localStorage.clear();
          resolve(false);
        } else {
          resolve(false);
        }
      });
    });
  };

  logout = (): Promise<void> => {
    return new Promise(() => {
      this._accessToken = undefined;
      this._idToken = undefined;
      this._expiresAt = 0;

      localStorage.removeItem(this._accessTokenKey);
      localStorage.removeItem(this._idTokenKey);
      localStorage.removeItem("isLoggedIn");

      this.auth0.logout({
        returnTo: window.location.origin
      });
    });
  };

  isAuthenticated() {
    // Check whether the current time is past the
    // access token's expiry time
    let expiresAt = this._expiresAt;
    return new Date().getTime() < expiresAt;
  }
}

export const authService = new AuthService();

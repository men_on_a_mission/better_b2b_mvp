import axios, { AxiosResponse } from 'axios';
import { API_URL } from '../constants';
import { authService } from './auth-service';

class ApiService {
  private _client = axios.create({
    baseURL: API_URL
  })

  async get<T>(url: string): Promise<AxiosResponse<T>> {
    const token = authService.getAccessToken();

    let response;
    try {
      response = await this._client.get<T>(url, {
        headers: {"Authorization": `Bearer ${token}`}
      });
    } catch (error) {
      response = error.response;
    }

    return response;
  }

  async post<T>(url: string, body: any): Promise<AxiosResponse<T>> {
    const token = authService.getAccessToken();

    let response;
    try {
      response = await this._client.post(url, body, {
        headers: {"Authorization": `Bearer ${token}`}
      });
    } catch (error) {
      response = error.response;
    }

    return response;
  }

  async delete(url: string): Promise<AxiosResponse> {
    const token = authService.getAccessToken();

    let response;
    try {
      response = await this._client.delete(url, {
        headers: {"Authorization": `Bearer ${token}`}
      });
    } catch (error) {
      response = error.response;
    }

    return response;
  }

  async put<T>(url: string, body: any): Promise<AxiosResponse<T>> {
    const token = authService.getAccessToken();

    let response;
    try {
      response = await this._client.put(url, body, {
        headers: {"Authorization": `Bearer ${token}`}
      });
    } catch (error) {
      response = error.response;
    }

    return response;
  }
}

export const apiService = new ApiService();
// Sorts string ascending
export function sortStringAsc(data, filter) {
    return [...data].sort((a, b) => {
        let A = a[filter].toLowerCase(), B = b[filter].toLowerCase()
        return A < B ? -1 : A > B ? 1 : 0
    })
}

// Sorts string descending
export function sortStringDesc(data, filter) {
    return [...data].sort((a, b) => {
        let A = a[filter].toLowerCase(), B = b[filter].toLowerCase()
        return A > B ? -1 : A < B ? 1 : 0
    })
}

// Sorts number descending
export function sortNumAsc(data, filter) {
    return [...data].sort((a, b) => {
        let A = parseInt(a[filter]), B = parseInt(b[filter])
        return A < B ? -1 : A > B ? 1 : 0
    })
}

// Sorts number descending
export function sortNumDesc(data, filter) {
    return [...data].sort((a, b) => {
        let A = parseInt(a[filter]), B = parseInt(b[filter])
        return A > B ? -1 : A < B ? 1 : 0
    })
}


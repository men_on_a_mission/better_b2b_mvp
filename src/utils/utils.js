import { message } from 'antd'
import { t } from 'ttag'

export function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
            message.success(t`Profile image uploaded`);
            resolve(reader.result)
        }
        reader.onerror = error => reject(error);
    });
}
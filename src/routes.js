import * as React from "react";
import StaffMembers from "./Views/StaffMembers";
import Visitors from "./Views/Visitors";
import Services from "./Views/Services";
import StaffMemberProfile from "./Views/StaffMemberProfile";
import MyProfile from "./Views/MyProfile";
import MySchedule from "./Views/MySchedule";
import Onboarding from "./Pages/NewUserOnboarding";
import Authentication from "./Pages/Authentication";
import SalonSetup from "./Pages/SalonSetup";
import { Switch, Route, Redirect } from "react-router-dom";
import DefaultLayout from "./Layouts/DefaultLayout";
import EmailVerification from "./Pages/EmailVerification";
import ProtectedRoute from "./protected-route";

// TODO: add Service view
// TODO: do inventory view

export const appRoutes = (isLoggedIn) => (
  <Switch>
    <ProtectedRoute
      path="/home"
      isLoggedIn={isLoggedIn}
      render={({ match }) =>
        <DefaultLayout>
          <Switch>
            <Route path={`${match.url}/staffMembers`} component={StaffMembers} />
            <Route path={`${match.url}/services`} component={Services} />
            <Route path={`${match.url}/visitors`} component={Visitors} />
            <Route path={`${match.url}/schedule`} component={MySchedule} />
            <Route path={`${match.url}/sfProfile`} component={StaffMemberProfile} />
            <Route path={`${match.url}/myProfile`} component={MyProfile} />
          </Switch>
        </DefaultLayout>
      }
    />
    <ProtectedRoute isLoggedIn={isLoggedIn} path="/salonSetup" component={SalonSetup} />
    <ProtectedRoute isLoggedIn={isLoggedIn} path="/signup" render={() => <Redirect to="/home" />}/>

    <Route path="/authentication" render={(props) =>
        !isLoggedIn ? <Authentication {...props} /> : <Redirect to="home" />} />
    <Route
      path="/emailVerification"
      render={(props) =>
        !isLoggedIn ? <EmailVerification {...props} /> : <Redirect to="home" />} />

    <Route render={() => <Redirect to="/home" />} />
  </Switch>
);

var routes = [
  {
    path: "/staffMembers",
    layout: "/home",
    name: "StaffMembers",
    icon: "user",
    component: StaffMembers
  },
  {
    path: "/visitors",
    layout: "/home",
    name: "Visitors",
    icon: "user",
    component: Visitors
  },
  {
    path: "/services",
    layout: "/home",
    name: "Services",
    icon: "tags",
    component: Services
  },
  {
    path: "/sfProfile",
    layout: "/home",
    name: "StaffMemberProfile",
    icon: "user",
    component: StaffMemberProfile
  },
  {
    path: "/authentication",
    layout: "",
    name: "Вход / Регистрация",
    icon: null,
    component: Authentication
  },
  {
    path: "/newUserOnboarding",
    layout: "",
    name: "Конфигуриране",
    icon: null,
    component: Onboarding
  },
  {
    path: "/salonRegister",
    layout: "",
    name: "Salon Registration",
    icon: null,
    component: SalonSetup
  }
];
export default routes;

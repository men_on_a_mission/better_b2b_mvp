export let API_URL:string;

switch (process.env.NODE_ENV) {
  // Only 'prod' concerns the deployment of Dashboard
  case "production":
    API_URL = 'hhttps://betterwebapi20190709084105.azurewebsites.net/api/';
    break;
  case 'development':
    API_URL = 'https://betterwebapi20190709084105.azurewebsites.net/api/';
    break;
  default:
    API_URL = 'https://betterwebapi20190709084105.azurewebsites.net/api/';
    break;
};
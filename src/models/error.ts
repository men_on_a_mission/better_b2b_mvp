export interface Error {
  code: number;
  detail: string;
}
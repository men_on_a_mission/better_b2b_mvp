export interface Visitor {
   name: string;
   email: string;
   phoneNumber: string;
}
export interface User {
   fullName: string;
   email: string;
   password: string;
   confirmPassword: string;
   phone: string;
   agreement: boolean;
}
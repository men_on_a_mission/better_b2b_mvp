import { createStore, applyMiddleware, ReducersMapObject, combineReducers, Store } from 'redux';
import { routerMiddleware, connectRouter } from 'connected-react-router';
import { reducers, State } from './store';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import * as StoreModule from './store';
import { History } from 'history';

export default function configureStore(history: History, initialState: State = {} as State) {
  const middleware = applyMiddleware(thunkMiddleware, routerMiddleware(history));
  const createStoreWithMiddleware = composeWithDevTools(middleware)(createStore);

  const allReducers = buildRootReducer(reducers, history);
  const store = createStoreWithMiddleware(allReducers, initialState) as Store<State>;

  if (process.env.NODE_EnNV !== "production" && module.hot) {
    module.hot.accept('./store', () => {
      const nextRootReducer = require<typeof StoreModule>('./store');
      store.replaceReducer(buildRootReducer(nextRootReducer.reducers, history));
    });
  }

  return store
}

function buildRootReducer(allReducers: ReducersMapObject, history:History) {
  return combineReducers<State>(Object.assign({}, allReducers, { router: connectRouter(history)}));
}
export default {
    PRIMARY: '#FC3F00',
    ORANGE: '#FC3F00',
    BLUE: '#4B5DFF',
    GREEN: '#1AB074',
    YELLOW: '#FDAA1C',
    PINK: '#E23380',
    WHITE: '#FFFFFF',
    TITLE: 'rgba(0, 0, 0, .85)',
    PRIMARY_TEXT: 'rgba(0, 0, 0, .65)',
    SECONDARY_TEXT: 'rgba(0, 0, 0, .45)',
    DISABLE: 'rgba(0, 0, 0, .25)',
    BORDER: 'rgba(0, 0, 0, .15)',
    DIVIDERS: 'rgba(0, 0, 0, .09)',
    BACKGROUND: 'rgba(0, 0, 0, .04)',
    TABLE_HEADER: 'rgba(249, 62, 0, 0.02)'
}
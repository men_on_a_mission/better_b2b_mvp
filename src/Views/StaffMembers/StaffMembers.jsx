import React, { Component } from "react";
import "./styles.scss";
import {
  Table, Button, Popconfirm, Form, message,
  notification, Row, Col, Typography
} from "antd";
import { DefaultButton, DefaultCard } from '../../components'
import WrappedStaffMemberFormModal from '../StaffMemberProfile/StaffMemberFormModal'

import { withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { salonId } from "../../models/user-claims";
import Animate from "rc-animate";
import {
  getStaffMembers,
  deleteStaffMember,
  createStaffMember,
  inviteStaffMember
} from "../../services/better-api-service";
import _colors from "../../custom_theme/_colors";
import { t } from 'ttag'

const { Title } = Typography;

class StaffMembers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      loading: false,
      visible: false,
      goToProfile: false,
      profileId: ''
    };
  }

  columns = [
    { title: t`Full name`, dataIndex: "fullName", width: "15%" },
    { title: t`E-mail`, dataIndex: "email", width: "20%" },
    { title: t`Phone number`, dataIndex: "phoneNumber", width: "20%" },
    { title: t`Position`, dataIndex: "position", width: "15%" },
    { title: t`Salary`, dataIndex: "salary", width: "15%" },
    {
      dataIndex: "open",
      render: (text, record) =>
        this.state.dataSource.length >= 1 ? (
          <Button onClick={() => this.openProfile(record)}>{t`Open`}</Button>
        ) : null
    },
    {
      dataIndex: "invitation",
      render: (text, record) =>
        this.state.dataSource.length >= 1 ? (
          <Button
            type="link"
            onClick={() => this.handleInvite(record)}
            loading={this.state.loading}
          >
            {t`Invite`}
          </Button>
        ) : null
    },
    {
      dataIndex: "delete",
      render: (text, record) =>
        this.state.dataSource.length >= 1 ? (
          <Popconfirm
            title={t`Are you sure?`}
            onConfirm={() => this.handleDelete(record.id)}
            okText={t`Yes`}
            cancelText={t`Cancel`}
          >

            <Button type="danger">{t`Delete`}</Button>
          </Popconfirm>
        ) : null
    }
  ];

  async componentDidMount() {
    this.setState({ loading: true });
    const id = this.props.user[salonId];
    const staffMembers = await getStaffMembers(id);

    if (staffMembers) {
      const displayedStaffMembers =
        staffMembers.map(member => ({
          ...member,
          fullName: `${member.firstName} ${member.lastName}`
        }))
      this.setState({ loading: false, dataSource: displayedStaffMembers });
    } else {
      this.setState({ loading: false });
    }
  }

  toggleModal = () => {
    this.setState(prevState => ({ visible: !prevState.visible }));
  };

  submitNewStaffMember = async (staffMember) => {
    this.setState({ loading: true });

    const id = this.props.user[salonId];
    const newStaffMember = await createStaffMember(id, staffMember);

    if (newStaffMember) {
      const displayedStaffMember = {
        ...newStaffMember,
        fullName: `${newStaffMember.firstName} ${newStaffMember.lastName}`
      }
      const { dataSource } = this.state;
      this.setState(
        { dataSource: [...dataSource, displayedStaffMember] },
        () => message.success(t`Team Member successfuly added!`)
      );
    }
    this.setState({ loading: false });
    this.toggleModal()
  };

  handleInvite = async staffMember => {
    const idSalon = this.props.user[salonId];
    const response = await inviteStaffMember(idSalon, staffMember);
    if (response) {
      notification.success({
        message: t`Success`,
        description: t`Invitation has been sent to the team member's e-mail!`
      });
    }
  };

  handleDelete = async id => {
    const { user } = this.props;
    if (id === user.id) message.error(t`You cannot delete yourself!`);

    const idSalon = this.props.user[salonId];
    await deleteStaffMember(idSalon, id);

    const { dataSource } = this.state;
    const staffMembers = dataSource.filter(sm => sm.id !== id);

    this.setState({ dataSource: staffMembers });
    message.success(t`Team Member successfuly deleted!`);
  };

  openProfile = (record) => {
    this.setState({ goToProfile: true, profileId: record.id })
  }

  render() {
    const { dataSource, loading, visible, goToProfile, profileId } = this.state;

    if (goToProfile) {
      return <Redirect to={{
        pathname: '/home/sfProfile',
        state: { id: profileId }
      }}
      />
    }
    return (
      <Animate transitionAppear transitionLeave transitionName="fade">
        <div className="mainViewContainer">
          <div className='mainViewHeader'>
            <Title style={{ fontWeight: '100' }}>{t`Team Members`}</Title>
            <DefaultButton onClick={this.toggleModal} text={t`New Team Member`} width='200px' />
          </div>

          <div className='contentWrapper shadowLight'>
            <Table loading={loading} bordered={false}
              dataSource={dataSource} columns={this.columns} pagination={{ pageSize: 5 }}
            />
          </div>

          <div className='staffMembersCards'>
            <Row gutter={24}>
              <Col span={6}>
                <DefaultCard color={_colors.BLUE} title={t`Currently Active`} info={t`Here is some information about the card`} height="200px">
                  <DefaultButton onClick={this.toggleModal} color={_colors.BLUE} text={t`New team member`} />
                </DefaultCard>
              </Col>

              <Col span={6}>
                <DefaultCard color={_colors.YELLOW} title={t`Currently Active`} info={t`Here is some information about the card`} height="200px">
                  <DefaultButton onClick={this.toggleModal} color={_colors.YELLOW} text={t`New team member`} />
                </DefaultCard>
              </Col>

              <Col span={6}>
                <DefaultCard color={_colors.GREEN} title={t`Currently Active`} info={t`Here is some information about the card`} height="200px">
                  <DefaultButton onClick={this.toggleModal} color={_colors.GREEN} text={t`New team member`} />
                </DefaultCard>
              </Col>

              <Col span={6}>
                <DefaultCard color={_colors.PINK} title={t`Currently Active`} info={t`Here is some information about the card`} height="200px">
                  <DefaultButton onClick={this.toggleModal} color={_colors.PINK} text={t`New team member`} />
                </DefaultCard>
              </Col>
            </Row>
          </div>
          <WrappedStaffMemberFormModal mode='new' visible={visible} toggle={this.toggleModal} submit={this.submitNewStaffMember} />
        </div>

      </Animate>
    );
  }
}

const WrappedStaffMembers = Form.create({ name: "coordinated" })(StaffMembers);

export default connect(
  state => state.user,
  null
)(withRouter(WrappedStaffMembers));

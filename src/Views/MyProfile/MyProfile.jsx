import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { t } from 'ttag'

import "./styles.scss";
import { Row, Col, Card, Avatar, Divider, Typography } from "antd";
import Animate from 'rc-animate';

import DefaultButton from '../../components/DefaultButton'

const { Title } = Typography

class MyProfile extends Component {
  render() {
    let user = this.props.user.user || {};
    return (
      <Animate transitionAppear transitionLeave transitionName="fade" exclusive={true}>
        <div className="mainViewContainer">
          <div className='mainViewHeader'>
            <Title style={{ fontWeight: '100' }}>{t`My profile`}</Title>
            <DefaultButton onClick={this.showModal} text={t`Edit`} width='200px' />

          </div>
          <Row>
            <Col md={4} xl={3}>
              <Card title={t`Profile Details`} className="profile-details">
                <Avatar size={128} shape="circle" src={user.picture} />
                <h3 style={{ marginTop: "10px" }}>{user.nickname}</h3>
                <Divider />
                <b>{t`About`}</b>
                <h4>{t`Salon:`} {this.props.common.salon.name}</h4>
              </Card>
            </Col>
          </Row>
        </div>
      </Animate>
    );
  }
}

export default connect(
  state => state,
  null
)(withRouter(MyProfile));

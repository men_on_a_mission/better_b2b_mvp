import React, { Component } from "react";
import "./styles.scss";
import { Form, Row, Col, Typography, message } from "antd";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Animate from "rc-animate";
import _colors from "../../custom_theme/_colors";
import DefaultButton from '../../components/DefaultButton'
import DefaultCard from '../../components/DefaultCard'
import WrappedVisitorFormModal from './VisitorProfileFormModal'
import profileImage from '../../assets/images/profilePic.png'
import { t } from 'ttag'
import { salonId } from "../../models/user-claims";

import { getGetVisitor, editVisitor } from "../../services/better-api-service";

const { Title, Text } = Typography;
class VisitorProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      visitor: {}
    };
  }

  async componentDidMount() {
    this.setState({ loading: true });
    const id = this.props.user[salonId];
    if (!this.props.location.state.id) return
    const visitor = await getVisitor(id, this.props.location.state.id);

    if (visitor) {
      this.setState({ loading: false, visitor });
    } else {
      this.setState({ loading: false });
    }
  }

  toggleModal = () => {
    this.setState(prevState => ({ visible: !prevState.visible }));
  };

  submitEditVisitor = async ( visitor ) => {
    this.setState({ loading: true });
    const id = this.props.user[salonId];

    const newVisitor = await editVisitor(id, this.state.visitor.id, visitor);    
    if (newVisitor) {
      this.setState({ visitor: newVisitor },
        () => message.success(t`Staff member successfuly modified!`));
    }
    this.setState({ loading: false });
    this.toggleModal()
  }

  render() {
    let { visitor } = this.state
    return (
      <Animate transitionAppear transitionLeave transitionName="fade">

        <div className="mainViewContainer">
          <div className='mainViewHeader'>
            <Title style={{ fontWeight: '100' }}>{t`EMPLOYEE POROFILE`}</Title>
            <DefaultButton color={_colors.ORANGE} onClick={this.toggleModal} text={t`Edit`} width='200px' />
          </div>

          <div className='contentWrapper'>
            {/* <Title className='secondaryTitle' level={2}> Personal Information </Title> */}
            <div className='personalInfoWrapper'>
              <img alt={t`Profile`} className='userImage' src={profileImage} />
              <div className='userInfoContainer'>
                <Title className='noMargin' level={3}>{`${visitor.firstName} ${visitor.lastName}`}</Title>
                <Text style={{ color: _colors.ORANGE, fontWeight: '600' }}>{visitor.position}</Text>

                <div className='employeeDetails'>
                  <Text if='smpTitle' className='edTitle'>{t`EMPLOYEE DETAILS`}</Text>
                  <EmployeeDetailsRow label={t`E-mail`} value={visitor.email} />
                  <EmployeeDetailsRow label={t`Phone number`} value={visitor.phoneNumber} />
                  <EmployeeDetailsRow label={t`Salary`} value={visitor.salary} />
                </div>
              </div>
            </div>
          </div>

          <div className='visitorCards'>
            <Row gutter={16}>
              <Col span={6}>
                <DefaultCard color={_colors.BLUE} title={t`Customers Served`} info={t`Here is some information about the card`} height="200px" style={{ marginRight: '16px' }} >
                  {/* <DefaultButton onClick={this.toggleModal} color={_colors.BLUE} text='Customers Served' /> */}
                </DefaultCard>
              </Col>

              <Col span={6}>
                <DefaultCard color={_colors.YELLOW} title={t`Total Revenue`} info={t`Here is some information about the card`} height="200px" style={{ marginRight: '16px' }} >
                  {/* <DefaultButton onClick={this.toggleModal} color={_colors.YELLOW} text='Total Revenue' /> */}
                </DefaultCard>
              </Col>

              <Col span={6}>
                <DefaultCard color={_colors.GREEN} title={t`Services Performed`} info={t`Here is some information about the card`} height="200px" style={{ marginRight: '16px' }} >
                  {/* <DefaultButton onClick={this.toggleModal} color={_colors.GREEN} text='Services Performed' /> */}
                </DefaultCard>
              </Col>

              <Col span={6}>
                <DefaultCard color={_colors.PINK} title={t`Currently Active`} info={t`Here is some information about the card`} height="200px" style={{ marginRight: '16px' }} >
                  <DefaultButton onClick={this.toggleModal} color={_colors.PINK} text={t`New team member`} />
                </DefaultCard>
              </Col>
            </Row>
          </div>
          <WrappedVisitorFormModal visible={this.state.visible} toggle={this.toggleModal} submit={this.submitEditVisitor}/>
        </div>
      </Animate>
    );
  }
}

const EmployeeDetailsRow = props => (
  <Row style={{ marginTop: '8px' }} gutter={16}>
    <Col span={10}>
      <Text className='edLabel'>{props.label}</Text>
    </Col>

    <Col span={12}>
      <Text className='edValue'>{props.value}</Text>
    </Col>
  </Row>
)

const WrappedVisitorProfile = Form.create({ name: "coordinated" })(VisitorProfile);

export default connect(
  state => state.user,
  null
)(withRouter(WrappedVisitorProfile));

import React, { Component } from "react";
import "./styles.scss";
import { Input, Button, Form, Modal } from "antd";
import { t } from 'ttag'

const { Item } = Form;

class VisitorFormModal extends Component {
    constructor(props) {
        super(props);
        this.state = { };
    }

    handleCancel = () => {
        this.props.form.resetFields();
        this.setState({ visible: false });
    };

    handleSubmit = async () => {
      await this.props.form.validateFields(async (err, visitor) => {
          if (!err) {
              this.props.submit(visitor)
              this.props.form.resetFields();
          }
      });
    };

    render() {

        const { getFieldDecorator } = this.props.form;
        const { visible } = this.props;
        return (
            <Modal
                visible={visible}
                title={mode === 'edit' ? t`Edit Visitor Details` : t`Edit Visitor Details`}
                onOk={this.handleSubmit}
                onCancel={this.handleCancel}
                footer={[
                    <Button key="back" onClick={this.handleCancel}>
                        {t`Cancel`}
                    </Button>,
                    <Button
                        key="submit"
                        type="primary"
                        onClick={this.handleSubmit}>
                        {t`Add`}
                    </Button>
                ]}
            >
                <Form layout='vertical' className='addStaffForm rowNoMargin'>
                    <Item label={t`Full name`}>
                        {getFieldDecorator("name", {
                            rules: [
                                { required: true, message: t`Please enter full name` }
                            ]
                        })(<Input />)}
                    </Item>
                    <Item label={t`Phone number`}>
                        {getFieldDecorator("phoneNumber", {
                            rules: [
                                { required: true, message: t`Please enter phone number` }
                            ]
                        })(<Input />)}
                    </Item>
                    <Item label={t`E-mail`}>
                        {getFieldDecorator("email", {
                            rules: [
                                { required: true, message: t`Please enter email` }
                            ]
                        })(<Input />)}
                    </Item>
                </Form>
            </Modal>
        );
    }
}

const WrappedVisitorFormModal = Form.create({ name: "coordinated" })(VisitorFormModal);

export default WrappedVisitorFormModal

import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import "./styles.scss";
import { Row, Col, Typography, Button } from "antd";
import Animate from 'rc-animate';
import { t } from 'ttag'

// Scheduler
import {
  Scheduler, DayView, WeekView, MonthView, Appointments,
  Toolbar, ViewSwitcher, DateNavigator, AppointmentTooltip,
  AppointmentForm, DragDropProvider, AllDayPanel
} from '@devexpress/dx-react-scheduler-material-ui';
import { ViewState, EditingState } from '@devexpress/dx-react-scheduler';
import {
  Paper, Dialog, DialogActions, DialogContent, DialogContentText,
  DialogTitle
} from '@material-ui/core';
// import Close from '@material-ui/icons/Close';

import DefaultButton from '../../components/DefaultButton'

const { Title } = Typography

const appointments = [
  { startDate: '2019-8-25 10:00', endDate: '2019-8-25 11:00', title: 'Meeting', id: '0' },
  { startDate: '2019-8-25 18:00', endDate: '2019-8-25 19:30', title: 'Go to a gym', id: '1' },
]
const dragDisableIds = new Set([]);

const allowDrag = ({ id }) => !dragDisableIds.has(id);
const DeleteConfirmationDialog = ({ visible, onCancel, onCommit }) => (
  <Dialog open={visible} >
    <DialogTitle>
      Delete Appointment
  </DialogTitle>
    <DialogContent>
      <DialogContentText>
        Are you sure you want to delete this appointment?
    </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button onClick={onCancel} color="secondary">
        Cancel
    </Button>
      <Button onClick={onCommit} color="danger">
        Delete
    </Button>
    </DialogActions>
  </Dialog>
);

class MySchedule extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      data: appointments,
      currentDate: new Date().toJSON().slice(0, 10),
      deletedAppointmentId: null,
      confirmationVisibility: false,
    };
    this.commitChanges = this.commitChanges.bind(this);
  }

  setDeletedAppointmentId = deletedAppointmentId => {
    this.setState({ deletedAppointmentId });
  }

  toggleConfirmationVisibility = () => {
    this.setState(prevState => ({
      confirmationVisibility: !prevState.confirmationVisibility
    }));
  }

  commitDeletedAppointment = () => {
    this.setState(prevState => {
      const { data, deletedAppointmentId } = prevState;
      const nextData = data.filter(appointment => appointment.id !== deletedAppointmentId);
      this.toggleConfirmationVisibility();
      return { data: nextData, deletedAppointmentId: null };
    });
  }

  commitChanges = ({ added, changed, deleted }) => {

    this.setState((state) => {
      let { data } = state;
      if (added) {
        const startingAddedId = data.length > 0 ? data[data.length - 1].id + 1 : 0;
        data = [...data, { id: startingAddedId, ...added }];
      }
      if (changed) {
        data = data.map(appointment => (
          changed[appointment.id] ? { ...appointment, ...changed[appointment.id] } : appointment));
      }
      if (deleted) {
        this.setDeletedAppointmentId(deleted);
        this.toggleConfirmationVisibility();
      }
      return { data };
    });
  }

  currentViewNameChange = (currentViewName) => {
    this.setState({ currentViewName });
  }

  render() {
    const { data, confirmationVisibility } = this.state;
    return (
      <Animate transitionAppear transitionLeave transitionName="fade" exclusive={true}>
        <div className="mainViewContainer">
          <div className='mainViewHeader'>
            <Title style={{ fontWeight: '100' }}>{t`My schedule`}</Title>
            <DefaultButton onClick={this.showModal} text={t`New appointment`} width='200px' />
          </div>

          <Row>
            <Col>
              <Paper>
                <Scheduler data={data} >
                  <ViewState />

                  {/* Day/Week/Month view */}
                  <WeekView startDayHour={9} endDayHour={20} />
                  <MonthView />
                  <DayView />

                  {/* Adds a toolbar */}
                  <Toolbar />

                  {/* Controls to change the calendar view day/week/month */}
                  <ViewSwitcher />

                  {/* Appointment component */}
                  <Appointments />

                  {/* SHows controls to change the date */}
                  <DateNavigator />

                  <EditingState onCommitChanges={this.commitChanges} />

                  <AppointmentTooltip showOpenButton showDeleteButton showCloseButton/>

                  <AppointmentForm />

                  <AllDayPanel />

                  <DragDropProvider allowDrag={allowDrag} />
                </Scheduler>

                <DeleteConfirmationDialog
                  visible={confirmationVisibility}
                  onCancel={this.toggleConfirmationVisibility}
                  onCommit={this.commitDeletedAppointment}
                />
              </Paper>
            </Col>
          </Row>
        </div>
      </Animate>
    );
  }
}

export default connect(
  state => state,
  null
)(withRouter(MySchedule));

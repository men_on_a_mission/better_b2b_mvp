import React from 'react';
import {
   Form, Input, Tooltip, Icon, Select, Row, Col, Checkbox, Button,
} from 'antd';
import './styles.scss'
import { withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActionCreators } from '../../store/user-store';
import { t } from 'ttag'

const { Option } = Select;

class RegistrationForm extends React.Component {
   state = {
      confirmDirty: false,
      autoCompleteResult: [],
      toHome: false
   };

   handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
         if (!err) {
            this.setState({toHome: true})
            this.props.setUser(values)
            console.log('Received values of form: ', values);
         }
      });
   }

   handleConfirmBlur = (e) => {
      const value = e.target.value;
      this.setState({ confirmDirty: this.state.confirmDirty || !!value });
   }

   compareToFirstPassword = (rule, value, callback) => {
      const form = this.props.form;
      if (value && value !== form.getFieldValue('password')) {
         callback(t`The passwords do not match`);
      } else {
         callback();
      }
   }

   validateToNextPassword = (rule, value, callback) => {
      const form = this.props.form;
      if (value && this.state.confirmDirty) {
         form.validateFields(['confirmPassword'], { force: true });
      }
      callback();
   }

   handleWebsiteChange = (value) => {
      let autoCompleteResult;
      if (!value) {
         autoCompleteResult = [];
      } else {
         autoCompleteResult = ['.com', '.org', '.net'].map(domain => `${value}${domain}`);
      }
      this.setState({ autoCompleteResult });
   }

   render() {

      if (this.state.toHome === true) {
         return <Redirect to='/' />
      }
      const { getFieldDecorator } = this.props.form;

      const formItemLayout = {
         labelCol: {
            sm: { span: 24 },
            md: { span: 8 },
         },
         wrapperCol: {
            sm: { span: 24 },
            md: { span: 14 },
         },
      };
      const tailFormItemLayout = {
         wrapperCol: {
            sm: {
               span: 24,
               offset: 0,
            },
            md: {
               span: 14,
               offset: 8,
            },
         },
      };
      const prefixSelector = getFieldDecorator('prefix', {
         initialValue: '86',
      })(
         <Select style={{ width: 80 }}>
            <Option value="86">+359</Option>
            <Option value="87">+44</Option>
         </Select>
      );

      return (

         <Form {...formItemLayout} onSubmit={this.handleSubmit}
            style={{ textAlign: 'left' }}
         >
            <Form.Item className='register-form-item' label={t`Full name`}>
               {getFieldDecorator('fullName', {
                  rules: [{ required: true, message: t`Please enter your full name`, whitespace: true }],
               })(
                  <Input />
               )}
            </Form.Item>

            <Form.Item className='register-form-item' label={(
               <span>
                  Имейл&nbsp;
                     <Tooltip title={t`Your e-mail will be used as your personal identifier when entering the platform`}>
                     <Icon type="question-circle-o" />
                  </Tooltip>
               </span>
            )}>
               {getFieldDecorator('email', {
                  rules: [{
                     type: 'email', message: t`Invalid e-mail`,
                  }, {
                     required: true, message: t`Please enter your e-mail`,
                  }],
               })(
                  <Input />
               )}
            </Form.Item>

            <Form.Item className='register-form-item' label={t`Password`}>
               {getFieldDecorator('password', {
                  rules: [{
                     required: true, message: t`Please enter password`,
                  }, {
                     validator: this.validateToNextPassword,
                  }],
               })(
                  <Input type="password" />
               )}
            </Form.Item>

            <Form.Item className='register-form-item' label={t`Confirm your password`}>
               {getFieldDecorator('confirmPassword', {
                  rules: [{
                     required: true, message: t`Please re-enter your password`,
                  }, {
                     validator: this.compareToFirstPassword,
                  }],
               })(
                  <Input type="password" onBlur={this.handleConfirmBlur} />
               )}
            </Form.Item>

            <Form.Item className='register-form-item' label={t`Phone number`}>
               {getFieldDecorator('phone', {
                  rules: [{ required: true, message: t`Please enter phone number`}],
               })(
                  <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
               )}
            </Form.Item>

            <Form.Item className='register-form-item' label="Captcha"
               extra={t`We must make sure you are human :)`}>
               <Row gutter={8}>
                  <Col span={12}>
                     {getFieldDecorator('captcha', {
                        rules: [{ required: true, message: t`Please enter the captcha code`}],
                     })(
                        <Input />
                     )}
                  </Col>
                  <Col span={12}>
                     <Button style={{ width: '100%' }}>{t`Get new code`}</Button>
                  </Col>
               </Row>
            </Form.Item>

            <Form.Item className='register-form-item' {...tailFormItemLayout}>
               {getFieldDecorator('agreement', {
                  valuePropName: 'checked',
               })(
                  <Checkbox style={{ fontSize: "12px" }}>{t`I have read and agree to the terms and conditions`}</Checkbox>
               )}
            </Form.Item>
            <Form.Item  {...tailFormItemLayout}>
               <Button type="primary" htmlType="submit" style={{ width: '100%' }}>
                  {t`Register`}
               </Button>
            </Form.Item>
         </Form>
      );
   }
}

const Register = Form.create({ name: 'register' })(RegistrationForm);

export default connect(
   state => state.user,
   userActionCreators
)(withRouter(Register));
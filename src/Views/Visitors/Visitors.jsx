import React, { Component } from "react";
import "./styles.scss";
import { withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Animate from "rc-animate";
import { t } from 'ttag'
import {
  Button, Form, Row, Col, Typography, message, Icon, Dropdown, Menu
} from "antd";

// Custom components
import { DefaultButton, SearchBar, LoadingOverlay, VisitorCard, VisitorCardCompact } from '../../components'
import VisitorsFormModal from './VisitorsFormModal'

import {
  sortStringAsc, sortStringDesc, sortNumAsc, sortNumDesc
} from '../../utils/sortingFunctions'

import {
  getVisitors, createVisitor, editVisitor
} from "../../services/better-api-service";
import { salonId } from "../../models/user-claims";

const { Item } = Menu;
const { Title } = Typography;

class Visitors extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      displayedData: [],
      topVisitors: [],
      loading: false,
      visible: false,
      goToProfile: false,
      sortOption: { key: '0' },
      mode: '',
      toEdit: {}
    };
  }

  async componentDidMount() {
    this.setState({ loading: true });
    const id = this.props.user[salonId];

    const visitors = await getVisitors(id);
    // const visitors = []
    // for (let i = 0; i < 20; ++i) {
    //   visitors.push({
    //     visits: i,
    //     name: `Name: ${i}`,
    //     phoneNumber: '+359879606986',
    //     email: 'krustevgeorgi@yahoo.com'
    //   })
    // }

    if (visitors) {
      this.setState({
        loading: false,
        dataSource: visitors,
        displayedData: sortStringAsc(visitors, 'name'),
        topVisitors: sortNumDesc(visitors, 'visits').slice(0, 4)
      });
    }
    this.setState({ loading: false });
  }

  toggleModal = () => {
    this.setState(prevState => ({ visible: !prevState.visible }));
  };


  // Reloads the top visitors section
  loadTopVisitors(data = this.state.dataSource) {
    this.setState({
      topVisitors: sortNumDesc(data, 'visits').slice(0, 4)
    })
  }

  // Handles sorting, IMPORTANT: after sort set sortOption to the type
  // of sort performed so the app can auto sort the list if users added
  // or deleted
  handleSort = (e) => {
    this.setState({ loading: true })
    switch (e.key) {
      case '0':
        this.setState(prevState => ({ displayedData: sortStringAsc(prevState.dataSource, 'name') }))
        break;
      case '1':
        this.setState(prevState => ({ displayedData: sortStringDesc(prevState.dataSource, 'name') }))
        break;
      case '2':
        this.setState(prevState => ({ displayedData: sortNumAsc(prevState.dataSource, 'visits') }))
        break;
      case '3':
        this.setState(prevState => ({ displayedData: sortNumDesc(prevState.dataSource, 'visits') }))
        break;
      default:
    }
    this.setState({ loading: false, sortOption: { key: e.key } })
  }

  //Searches by name/email/phone number
  filterVisitors = (e) => {
    let searchTerm = e.target.value.toLowerCase();
    if (searchTerm.length < 2) {
      this.setState(prevState => ({ displayedData: prevState.dataSource }))
      return
    }

    this.setState(prevState => ({
      displayedData: [...prevState.dataSource].filter(visitor =>
        visitor.name.toLowerCase().includes(searchTerm) ||
        visitor.email.toLowerCase().includes(searchTerm) ||
        visitor.phoneNumber.toLowerCase().includes(searchTerm))
    }))
  }

  // Toggles edit modal
  toggleModal = (mode = 'edit', visitor = {}) => {
    this.setState(prevState => ({ visible: !prevState.visible, mode, toEdit: visitor }));
  };

  // On cancel pressed of edit modal
  handleCancel = () => {
    this.props.form.resetFields();
    this.setState({ visible: false });
  };

  // Creates a user
  handleCreateVisitor = async (visitor) => {
    this.setState({ loading: true });

    const id = this.props.user[salonId];
    const newVisitor = await createVisitor(id, visitor);
    if (newVisitor) {
      this.setState(prevState => ({ dataSource: [...prevState.dataSource, newVisitor] }),
        () => {
          message.success(t`Visitor successfuly created!`)
          this.handleSort(this.state.sortOption)
          this.loadTopVisitors()
        }
      );
      // profileImage: profileImage,
      // visits: rndm(200)
    }
    this.setState({ loading: false });
    this.toggleModal()
    this.props.form.resetFields();
  }

  handleEditVisitor = async (visitor) => {
    this.setState({ loading: true });
    const id = this.props.user[salonId];
    const { toEdit, sortOption } = this.state;

    const newVisitor = await editVisitor(id, toEdit.id, visitor);
    this.setState(prevState => ({
      dataSource: [...prevState.dataSource].map(visitor =>
        visitor.id === newVisitor.id ? newVisitor : visitor)
    }),
      () => {
        message.success(t`Visitor successfuly modified!`)
        this.handleSort(sortOption)
        this.loadTopVisitors()
      }
    );
    this.setState({ loading: false });
    this.toggleModal()
  }

  // Opens user profile
  openProfile = (id) => {
    this.setState({ goToProfile: true, profileId: id })
  }

  fetchData = () => {
    let displayedData = this.state.displayedData, count = this.state.displayedData.length;
    for(let i = count + 1; i <= count + 20; i++){
      displayedData.push({
        visits: i,
        name: `Name: ${i}`,
        phoneNumber: '+359879606986',
        email: 'krustevgeorgi@yahoo.com'
      })
    }
    this.setState({displayedData})
  }

  render() {
    const { topVisitors, displayedData, loading, visible, mode } = this.state;

    if (this.state.goToProfile) {
      return <Redirect to={{
        pathname: '/home/visitorProfile',
        state: { id: '123' }
      }}
      />
    }
    return (
      <Animate transitionAppear transitionLeave transitionName="fade">
        <div className="mainViewContainer">

          {loading && <LoadingOverlay />}

          <div className='mainViewHeader'>
            <Title style={{ fontWeight: '100' }}>{t`Visitors`}</Title>
            <DefaultButton onClick={() => this.toggleModal('new')} text={t`New visitor`} width='200px' />
          </div>

          <Row gutter={24}>
            {topVisitors.map((visitor, key) => (
              key < 4
                ? <Col xs={24} md={12} lg={8} xl={6} key={key}>
                  <VisitorCard edit={this.toggleModal} visitor={visitor} />
                </Col>
                : null
            ))}
          </Row>

          <Title level={3} className='visitorSectionTitle'>{t`All Visitors`}</Title>

          <div className='visitorsActionbar'>
            <SearchBar
              width='400px'
              textSize='1.4em'
              iconSize='1.4em'
              placeholder={t`Search for visitors...`}
              onChange={this.filterVisitors}
            />

            <Dropdown overlay={
              <Menu onClick={this.handleSort}>
                <Item key="0"> <Icon type='arrow-up' />{t`Full name`}</Item>
                <Item key="1"> <Icon type='arrow-down' />{t`Full name`} </Item>
                <Item key="2"> <Icon type='arrow-up' />{t`Visits`} </Item>
                <Item key="3"> <Icon type='arrow-down' />{t`Visits`} </Item>
              </Menu>
            }>
              <Button type='link'>
                <i className="fas fa-filter" style={{ fontSize: '1.3em' }}></i>
              </Button>
            </Dropdown>
          </div>

          <Row gutter={24}>
            {displayedData.map((visitor, key) => (
              <Col xs={24} md={12} lg={8} xl={6} key={key}>
                <VisitorCardCompact edit={this.toggleModal} visitor={visitor} />
              </Col>
            ))}
          </Row>
          <Row type='flex'>
            <Button type='dashed' onClick={this.fetchData} icon='arrow-down' style={{margin: '16px auto 0 auto' }}>
              {t`Load more`}
            </Button>
          </Row>

          <VisitorsFormModal mode={mode} visible={visible}
            toggle={this.toggleModal} submit={mode === 'new' ? this.handleCreateVisitor : this.handleEditVisitor}
          />
        </div>
      </Animate>
    );
  }
}

const WrappedVisitors = Form.create({ name: "coordinated" })(Visitors);

export default connect(
  state => state.user,
  null
)(withRouter(WrappedVisitors));

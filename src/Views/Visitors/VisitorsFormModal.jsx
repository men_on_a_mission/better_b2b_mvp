import React, {Component} from "react";
import "./styles.scss";
import 'cropperjs/dist/cropper.css';
import {Input, Button, Form, Modal, Upload, Icon, message, Card, Tooltip} from "antd";
import {t} from 'ttag'
import Cropper from 'react-cropper';
import {getBase64} from "../../utils/utils";

const {Item} = Form;
const {Dragger} = Upload;

class VisitorFormModal extends Component {
    constructor(props) {
        super(props);
        this.state = {previewImage: null, fileList: [], cropped: null};
        this.cropper = React.createRef(null);
    }

    _crop() {
    }

    handleCancel = () => {
        this.props.form.resetFields();
        this.setState({visible: false});
    };

    handleSubmit = async () => {
        await this.props.form.validateFields(async (err, visitor) => {
            if (!err) {
                this.props.submit(visitor)
                this.props.form.resetFields();
            }
        });
    };

    handlePreview = async file => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }
        this.setState({
            previewImage: file.url || file.preview
        });
    };

    handleChange = async ({fileList}) => {
        let file = fileList[0]
        if(!this.beforeUpload(file)) { return false }
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }
        this.setState({
            previewImage: file.url || file.preview
        });
        this.setState({fileList});
    }

    beforeUpload = (file) => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error(t`You can only upload JPG/PNG file!`);
            return false
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error(t`Image must smaller than 2MB!`);
            return false
        }
        return isJpgOrPng && isLt2M;
    }

    saveCropped = () => {
        const ref = this.cropper.current;
        let cropped = ref.cropper.getCroppedCanvas().toDataURL();
        this.setState({previewImage: null, cropped})
    }

    render() {

        const {getFieldDecorator} = this.props.form;
        const {visible, mode, toggle} = this.props;
        const {previewImage, cropped} = this.state;
        return (
            <Modal
                visible={visible}
                title={mode === 'edit' ? t`Edit Visitor Details` : t`Add New Visitor`}
                onOk={this.handleSubmit}
                onCancel={toggle}
                footer={[
                    <Button key="back" onClick={toggle}>
                        {t`Cancel`}
                    </Button>,
                    <Button
                        key="submit"
                        type="primary"
                        onClick={this.handleSubmit}>
                        {mode === 'edit' ? t`Save` : t`Add`}
                    </Button>
                ]}
            >
                {
                    cropped
                        ? <Card
                            className='profileImagePreviewCard'
                            style={{width: '150px', marginBottom: '16px', marginLeft: 'auto', marginRight: 'auto'}}
                            cover={<img alt="example" src={cropped}/>}
                            actions={[
                                <Tooltip title={t`Delete Profile Image`}>
                                    <Icon type="delete" key="delete" onClick={() => this.setState({cropped: null})}/>
                                </Tooltip>,
                            ]}>
                        </Card>
                        : <div style={{marginBottom: '16px'}}>
                            {
                                previewImage
                                    ? <div style={{display: 'flex', flexDirection: 'column'}}>
                                        < Cropper
                                            ref={this.cropper}
                                            src={previewImage}
                                            style={{height: '300px', width: '100%'}}
                                            aspectRatio={1 / 1}
                                            guides={false}
                                            crop={this._crop.bind(this)}/>
                                        <Button style={{marginTop: '8px'}} onClick={this.saveCropped}>Save</Button>
                                    </div>
                                    : <Dragger name='file' multiple={false}
                                               onPreview={this.handlePreview}
                                               showUploadList={false}
                                               beforeUpload={this.beforeUpload}
                                               onChange={this.handleChange}>
                                        <p className="ant-upload-drag-icon"><Icon type="inbox"/></p>
                                        <p className="ant-upload-text">{t`Click or drag to add a profile picture`}</p>
                                    </Dragger>

                            }
                        </div>
                }
                <Form layout='vertical' className='addStaffForm rowNoMargin'>
                    <Item label={t`Full name`}>
                        {getFieldDecorator("name", {
                            rules: [
                                {required: true, message: t`Please enter full name`}
                            ]
                        })(<Input/>)}
                    </Item>
                    <Item label={t`Phone number`}>
                        {getFieldDecorator("phoneNumber", {
                            rules: [
                                {required: true, message: t`Please enter phone number`}
                            ]
                        })(<Input/>)}
                    </Item>
                    <Item label={t`E-mail`}>
                        {getFieldDecorator("email", {
                            rules: [
                                {required: true, message: t`Please enter email`}
                            ]
                        })(<Input/>)}
                    </Item>
                </Form>
            </Modal>
        );
    }
}

const WrappedVisitorFormModal = Form.create({name: "coordinated"})(VisitorFormModal);

export default WrappedVisitorFormModal

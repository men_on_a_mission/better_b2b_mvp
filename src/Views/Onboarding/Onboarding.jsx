import React, { Component } from 'react';
import { Button, Steps, message } from 'antd';
import './styles.scss'

const Step = Steps.Step;

const steps = [{
   title: 'Лични данни',
   content: 'First-content',
}, {
   title: 'Предпочитания',
   content: 'Second-content',
}, {
   title: 'Условия',
   content: 'Last-content',
}];

class Onboarding extends Component {
   constructor(props) {
      super(props)

      this.state = {
         current: 0,
      }
   }

   next() {
      const current = this.state.current + 1;
      this.setState({ current });
   }

   prev() {
      const current = this.state.current - 1;
      this.setState({ current });
   }

   render() {
      const { current } = this.state

      return (
         <div>
            <Steps current={current}>
               {steps.map(item => <Step key={item.title} title={item.title} />)}
            </Steps>
            <div className="steps-content">{steps[current].content}</div>
            <div className="steps-action" style={{textAlign:"center"}}>
               {
                  current > 0
                  && (
                     <Button onClick={() => this.prev()}>
                        Назад
                     </Button>
                  )
               }
               {
                  current < steps.length - 1
                  && <Button style={{ marginLeft: 8 }} type="primary" onClick={() => this.next()}>Продължи</Button>
               }
               {
                  current === steps.length - 1
                  && <Button style={{ marginLeft: 8 }} type="primary" onClick={() => message.success('Регистрация успешна!')}>Готово</Button>
               }
            </div>
         </div>
      )
   }
}


export default Onboarding

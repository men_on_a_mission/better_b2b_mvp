import React, { Component } from "react";
import "./styles.scss";
import { t } from 'ttag'
import { Input, Button, Form, Modal } from "antd";

const { Item } = Form;

class ServiceFormModal extends Component {
    constructor(props) {
        super(props);
        this.state = { };
    }

    componentDidMount() { }

    handleSubmit = async () => {
        await this.props.form.validateFields(async (err, service) => {
            if (!err) {
                this.props.submit(service)
                this.props.form.resetFields();
            }
        });
    };

    handleCancel = () => {
        this.props.form.resetFields();
        this.props.toggle()
    };

    render() {
        const { visible } = this.props;
        const { getFieldDecorator } = this.props.form;

        return (
            <Modal
                visible={visible}
                title={t`Add New Service`}
                onOk={this.handleSubmit}
                onCancel={this.handleCancel}
                footer={[
                    <Button key="back" onClick={this.handleCancel}> {t`Cancel`} </Button>,
                    <Button
                        key="submit"
                        type="primary"
                        onClick={this.handleSubmit}
                    >
                        {t`Add`}
                    </Button>
                ]}
            >            <Form layout='vertical' className='addStaffForm rowNoMargin'>
                    <Item label={t`Name`}>
                        {getFieldDecorator("name", {
                            rules: [
                                { required: true, message: t`Please enter service name` }
                            ]
                        })(<Input />)}
                    </Item>
                    <Item label={t`Price`}>
                        {getFieldDecorator("price", {
                            rules: [
                                { required: true, message: t`Please enter service price` }
                            ]
                        })(<Input />)}
                    </Item>
                    <Item label={t`Description`}>
                        {getFieldDecorator("description", { rules: [{ required: false }] })(<Input />)}
                    </Item>
                </Form>
            </Modal>
        );
    }
}

const WrappedServiceFormModal = Form.create({ name: "coordinated" })(ServiceFormModal);

export default WrappedServiceFormModal

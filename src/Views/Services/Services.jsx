import React, { Component } from "react";
import "./styles.scss";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Animate from "rc-animate";
import { t } from 'ttag'
import {
  Table, Button, Popconfirm, message, Menu, Typography, Dropdown, Icon
} from "antd";

// Custom components
import { DefaultButton  } from '../../components'
import ServiceFormModal from './ServiceFormModal'

import { salonId } from "../../models/user-claims";
import {
  getServices, deleteService, createService, editService
} from "../../services/better-api-service";

import {
  sortStringAsc, sortStringDesc, sortNumAsc, sortNumDesc
} from '../../utils/sortingFunctions'

const { Title } = Typography;

class Services extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      loading: false,
      visible: false,
      goToProfile: false,
      toEditId: ''
    };
  }

  columns = [
    { title: t`Service name`, dataIndex: "name", width: "25%", align: 'center' },
    { title: t`Price`, dataIndex: "price", width: "20%", align: 'center' },
    { title: t`Duration`, dataIndex: "duration", width: "25%", align: 'center' },
    { title: t`Description`, dataIndex: "description", width: "25%", align: 'center' },
    {
      dataIndex: "edit",
      align: 'right',
      render: (text, record) =>
        this.state.dataSource.length >= 1 ? (
          <Button onClick={() => this.handleEdit(record)}>{t`Edit`}</Button>
        ) : null
    },
    {
      title: (sortOrder, filters) =>
        <FiltersDropdown handleSort={this.handleSort}>
          <Button type='link' style={{ float: 'right' }}>
            <i className="fas fa-filter" style={{ fontSize: '1.3em' }}></i>
          </Button>
        </FiltersDropdown>,
      dataIndex: "delete",
      align: 'right',
      render: (text, record) =>
        this.state.dataSource.length >= 1 ? (
          <Popconfirm
            title={t`Are you sure?`}
            onConfirm={() => this.handleDelete(record.id)}
            okText={t`Yes`}
            cancelText={t`Cancel`}
          >
            <Button type="danger">{t`Delete`}</Button>
          </Popconfirm>
        ) : null
    }
  ];

  async componentDidMount() {
    this.setState({ loading: true });
    const id = this.props.user[salonId];
    const services = await getServices(id);

    services && this.setState({ dataSource: services });
    
    this.setState({ loading: false });
  }

  handleSort = (e) => {
    let { dataSource } = this.state
    this.setState({ loading: true })
    let sorted = []

    if (e.key === '0') {
      sorted = sortStringAsc(dataSource, 'name')
    } else if (e.key === '1') {
      sorted = sortStringDesc(dataSource, 'name')
    } else if (e.key === '2') {
      sorted = sortNumAsc(dataSource, 'price')
    } else if (e.key === '3') {
      sorted = sortNumDesc(dataSource, 'price')
    } else if (e.key === '4') {
      sorted = sortStringAsc(dataSource, 'description')
    } else if (e.key === '5') {
      sorted = sortStringDesc(dataSource, 'description')
    }

    this.setState({ loading: false, dataSource: sorted })
  }

  toggleModal = (actionType = 'create') => {
    this.setState(prevState => ({ visible: !prevState.visible, actionType }));
  };

  handleCancel = () => {
    this.props.form.resetFields();
    this.setState({ visible: false });
  };

  getFormData = (data) => {
    if(this.state.actionType === 'create'){
      this.submitService(data)
    }else{
      this.submitEditService(data)
    }
  }

  submitEditService = async ( service ) => {
    this.setState({ loading: true });
    const id = this.props.user[salonId];

    const newService = await editService(id, this.state.toEditId, service);    
    if (newService) {

      this.setState(prevState => ({ dataSource: [...prevState.dataSource].map(service => 
        service.id === newService.id ? newService : service)}), 
        () => message.success(t`Service successfuly modified!`)
      );
    }
    this.setState({ loading: false });
    this.toggleModal()
  }

  handleEdit = (service) => {
    this.setState({toEditId: service.id})
    this.toggleModal('edit')
  }

  submitService = async (service) => {
    this.setState({ loading: true });

    const id = this.props.user[salonId];
    const newService = await createService(id, service);

    if (newService) {
      const { dataSource } = this.state;
      this.setState({ dataSource: [...dataSource, newService]},
        () => message.success(t`Service successfuly added!`)
      );
    }
    this.setState({ loading: false });
    this.toggleModal()
  }

  handleDelete = async id => {

    const idSalon = this.props.user[salonId];
    await deleteService(idSalon, id);

    const { dataSource } = this.state;
    const services = dataSource.filter(service => service.id !== id);

    this.setState({ dataSource: services });
    message.success(t`Service successfuly deleted!`);
  };

  render() {
    const { dataSource, loading, visible } = this.state;

    return (
      <Animate transitionAppear transitionLeave transitionName="fade">
        <div className="mainViewContainer">
          <div className='mainViewHeader'>
            <Title style={{ fontWeight: '100' }}>{t`Services View`}</Title>
            <DefaultButton onClick={() => this.toggleModal('create')} text={t`New Service`} width='200px' />
          </div>

          <div className='contentWrapper shadowLight'>
            <Table
              loading={loading}
              bordered={false}
              dataSource={dataSource}
              columns={this.columns}
              pagination={{ pageSize: 10 }}
            />
          </div>
          <ServiceFormModal visible={visible} toggle={this.toggleModal} submit={this.getFormData} />
        </div>
      </Animate >
    );
  }
}

const FiltersDropdown = (props) => {
  const { handleSort } = props

  return (
    <Dropdown placement='bottomCenter' overlay={
      <Menu onClick={handleSort}>
        <Menu.Item key="0"> <Icon type='arrow-up' />{t`Name`}</Menu.Item>
        <Menu.Item key="1"> <Icon type='arrow-down' />{t`Name`} </Menu.Item>
        <Menu.Item key="2"> <Icon type='arrow-up' />{t`Price`} </Menu.Item>
        <Menu.Item key="3"> <Icon type='arrow-down' />{t`Price`} </Menu.Item>
        <Menu.Item key="4"> <Icon type='arrow-up' />{t`Description`} </Menu.Item>
        <Menu.Item key="5"> <Icon type='arrow-down' />{t`Description`} </Menu.Item>
      </Menu>
    }>
      {props.children}
    </Dropdown>
  )
}

export default connect(
  state => state.user,
  null
)(withRouter(Services));

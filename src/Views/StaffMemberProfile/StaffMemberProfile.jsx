import React, { Component } from "react";
import "./styles.scss";
import { Form, Row, Col, Typography, message } from "antd";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Animate from "rc-animate";
import _colors from "../../custom_theme/_colors";
import DefaultButton from '../../components/DefaultButton'
import DefaultCard from '../../components/DefaultCard'
import WrappedStaffMemberFormModal from './StaffMemberFormModal'
import profileImage from '../../assets/images/profilePic.png'
import { t } from 'ttag'
import { salonId } from "../../models/user-claims";

import { getStaffMember, editStaffMember} from "../../services/better-api-service";

const { Title, Text } = Typography;
class StaffMemberProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      staffMember: {}
    };
  }

  async componentDidMount() {
    this.setState({ loading: true });
    const id = this.props.user[salonId];
    if (!this.props.location.state.id) return
    const staffMember = await getStaffMember(id, this.props.location.state.id);

    if (staffMember) {
      this.setState({ loading: false, staffMember });
    } else {
      this.setState({ loading: false });
    }
  }

  toggleModal = () => {
    this.setState(prevState => ({ visible: !prevState.visible }));
  };

  submitEditStaffMember = async ( staffMember ) => {
    this.setState({ loading: true });
    const id = this.props.user[salonId];

    const newStaffMember = await editStaffMember(id, this.state.staffMember.id, staffMember);    
    if (newStaffMember) {
      this.setState({ staffMember: newStaffMember },
        () => message.success(t`Staff member successfuly modified!`));
    }
    this.setState({ loading: false });
    this.toggleModal()
  }

  render() {
    let { staffMember } = this.state
    return (
      <Animate transitionAppear transitionLeave transitionName="fade">

        <div className="mainViewContainer">
          <div className='mainViewHeader'>
            <Title style={{ fontWeight: '100' }}>{t`EMPLOYEE POROFILE`}</Title>
            <DefaultButton color={_colors.ORANGE} onClick={this.toggleModal} text={t`Edit`} width='200px' />
          </div>

          <div className='contentWrapper'>
            {/* <Title className='secondaryTitle' level={2}> Personal Information </Title> */}
            <div className='personalInfoWrapper'>
              <img alt={t`Profile`} className='userImage' src={profileImage} />
              <div className='userInfoContainer'>
                <Title className='noMargin' level={3}>{`${staffMember.firstName} ${staffMember.lastName}`}</Title>
                <Text style={{ color: _colors.ORANGE, fontWeight: '600' }}>{staffMember.position}</Text>

                <div className='employeeDetails'>
                  <Text if='smpTitle' className='edTitle'>{t`EMPLOYEE DETAILS`}</Text>
                  <EmployeeDetailsRow label={t`E-mail`} value={staffMember.email} />
                  <EmployeeDetailsRow label={t`Phone number`} value={staffMember.phoneNumber} />
                  <EmployeeDetailsRow label={t`Salary`} value={staffMember.salary} />
                </div>
              </div>
            </div>
          </div>

          <div className='staffMembersCards'>
            <Row gutter={16}>
              <Col span={6}>
                <DefaultCard color={_colors.BLUE} title={t`Customers Served`} info={t`Here is some information about the card`} height="200px" style={{ marginRight: '16px' }} >
                  {/* <DefaultButton onClick={this.toggleModal} color={_colors.BLUE} text='Customers Served' /> */}
                </DefaultCard>
              </Col>

              <Col span={6}>
                <DefaultCard color={_colors.YELLOW} title={t`Total Revenue`} info={t`Here is some information about the card`} height="200px" style={{ marginRight: '16px' }} >
                  {/* <DefaultButton onClick={this.toggleModal} color={_colors.YELLOW} text='Total Revenue' /> */}
                </DefaultCard>
              </Col>

              <Col span={6}>
                <DefaultCard color={_colors.GREEN} title={t`Services Performed`} info={t`Here is some information about the card`} height="200px" style={{ marginRight: '16px' }} >
                  {/* <DefaultButton onClick={this.toggleModal} color={_colors.GREEN} text='Services Performed' /> */}
                </DefaultCard>
              </Col>

              <Col span={6}>
                <DefaultCard color={_colors.PINK} title={t`Currently Active`} info={t`Here is some information about the card`} height="200px" style={{ marginRight: '16px' }} >
                  <DefaultButton onClick={this.toggleModal} color={_colors.PINK} text={t`New team member`} />
                </DefaultCard>
              </Col>
            </Row>
          </div>
          <WrappedStaffMemberFormModal mode='edit' visible={this.state.visible} toggle={this.toggleModal} submit={this.submitEditStaffMember}/>
        </div>
      </Animate>
    );
  }
}

const EmployeeDetailsRow = props => (
  <Row style={{ marginTop: '8px' }} gutter={16}>
    <Col span={10}>
      <Text className='edLabel'>{props.label}</Text>
    </Col>

    <Col span={12}>
      <Text className='edValue'>{props.value}</Text>
    </Col>
  </Row>
)

const WrappedStaffMemberProfile = Form.create({ name: "coordinated" })(StaffMemberProfile);

export default connect(
  state => state.user,
  null
)(withRouter(WrappedStaffMemberProfile));

import React, { Component } from "react";
import "./styles.scss";
import { Input, Button, Form, Modal } from "antd";
import { t } from 'ttag'

const { Item } = Form;

class StaffMemberFormModal extends Component {
    constructor(props) {
        super(props);
        this.state = { };
    }

    handleCancel = () => {
        this.props.form.resetFields();
        this.setState({ visible: false });
    };

    handleSubmit = async () => {
      await this.props.form.validateFields(async (err, staffMember) => {
          if (!err) {
              this.props.submit(staffMember)
              this.props.form.resetFields();
          }
      });
    };

    render() {

        const { getFieldDecorator } = this.props.form;
        const { visible, toggle, mode } = this.props;
        return (
            <Modal
                visible={visible}
                title={mode === 'edit' ? t`Edit Team Member Details` : t`Add New Team Member`}
                onOk={this.handleSubmit}
                onCancel={toggle}
                footer={[
                    <Button key="back" onClick={toggle}>
                        {t`Cancel`}
                    </Button>,
                    <Button
                        key="submit"
                        type="primary"
                        onClick={this.handleSubmit}>
                        {mode === 'edit' ? t`Save` : t`Add`}
                    </Button>
                ]}
            >
                <Form layout='vertical' className='addStaffForm rowNoMargin'>
                    <Item label={t`E-mail`}>
                        {getFieldDecorator("email", {
                            rules: [
                                { required: true, message: t`Please enter your e-mail` }
                            ]
                        })(<Input />)}
                    </Item>
                    <Item label={t`First name`}>
                        {getFieldDecorator("firstName", {
                            rules: [
                                { required: true, message: t`Please enter first name` }
                            ]
                        })(<Input />)}
                    </Item>
                    <Item label={t`Last name`}>
                        {getFieldDecorator("lastName", {
                            rules: [
                                { required: true, message: t`Please enter last name` }
                            ]
                        })(<Input />)}
                    </Item>
                    <Item label={t`Phone number`}>
                        {getFieldDecorator("phoneNumber", {
                            rules: [{ required: false}]
                        })(<Input />)}
                    </Item>
                    <Item label={t`Position`}>
                        {getFieldDecorator("position", {
                            rules: [
                                { required: true, message: t`Please enter position` }
                            ]
                        })(<Input />)}
                    </Item>
                    <Item label={t`Salary`}>
                        {getFieldDecorator("salary", {
                            rules: [
                                { required: false, message: t`Please enter salary` }
                            ]
                        })(<Input />)}
                    </Item>
                    {/* <Item label={t`Address`}>
                        {getFieldDecorator("address", {
                            rules: [
                                { required: true, message: t`Please enter address` }
                            ]
                        })(<Input />)}
                    </Item> */}
                </Form>
            </Modal>
        );
    }
}

const WrappedStaffMemberFormModal = Form.create({ name: "coordinated" })(StaffMemberFormModal);

export default WrappedStaffMemberFormModal

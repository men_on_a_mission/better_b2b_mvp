import React from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { Link } from 'react-router-dom';
import './styles.scss';
import { t } from 'ttag';


class LoginForm extends React.Component {
   constructor(props) {
      super(props)
      this.state = {}
   }

   handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
         if (!err) {
            console.log('Received values of form: ', values);
         }
      });
   }

   render() {
      const { getFieldDecorator } = this.props.form;
      return (
         <div style={{ textAlign: "center" }}>
            <Button type="primary" size='large' style={{ width: "100%" }}>
               <Icon type="facebook" theme="filled" /> Вход с Facebook
            </Button>
            <div style={{ margin: '16px 0' }}> <span>или</span> </div>
            <Form onSubmit={this.handleSubmit} className="login-form">
               <Form.Item className='login-form-item'>
                  {getFieldDecorator('email', {
                     rules: [{ required: true, message: t`Please enter email`}],
                  })(
                     <Input prefix={<Icon type="user" className='login-form-input-icon' />} placeholder={t`E-mail`}/>
                  )}
               </Form.Item>

               <Form.Item className='login-form-item'>
                  {getFieldDecorator('password', {
                     rules: [{ required: true, message: t`Please enter password` }],
                  })(
                     <Input prefix={<Icon type="lock" className='login-form-input-icon' />} type="password" placeholder={t`Password`}/>
                  )}
               </Form.Item>

               <Form.Item className='login-form-item'>
                  {getFieldDecorator('remember', {
                     valuePropName: 'checked',
                     initialValue: true,
                  })(
                     <Checkbox className="login-form-remember">{t`Remember me`}</Checkbox>
                  )}
                  <Link to='/'><Button type="primary" htmlType="submit" className="login-form-button">
                     {t`Log in`}
                  </Button></Link>
               <a className="login-form-forgot">
                  {t`Forgotten password`}
               </a>
               
               </Form.Item>
            </Form>
         </div >
      );
   }
}

const Login = Form.create({ name: 'normal_login' })(LoginForm);

export default Login

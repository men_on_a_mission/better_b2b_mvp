const CopyWebpackPlugin = require('copy-webpack-plugin');
const { override, fixBabelImports, addLessLoader } = require('customize-cra');

function myOverrides(config) {
  // do stuff to config
  return config
}

module.exports = override(
  myOverrides,
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true,
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: {
      '@primary-color': '#FC3F00',
    },
  }),
);